# react-inforvation-utility

[![NPM](https://img.shields.io/npm/v/react-inforvation-utility.svg)](https://www.npmjs.com/package/react-inforvation-utility) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-inforvation-utility
```

## Usage

```jsx
import React, { Component } from 'react'

#BlobImage
import { Grid,Segment } from 'semantic-ui-react'
import { BlobImage } from 'react-inforvation-utility'

class Example extends Component {
  render () {
    let token = 'TOKEN'
    return (
      <Grid columns={2}>
        <Grid.Column>
          <BlobImage 
            token={token} 
            account='ACCOUNT_NAME'            
            container='CONTAINER_NAME'
            name='FILE_NAME'
            width = '100%'  {/*Percent or Pixel*/} 
            height = '100%' {/*Percent or Pixel*/} 
            ribbonDescription = 'รูปที่ 1 ทดสอบ'
            ribbonColor = 'COLOR_LIBBON' {/*Example 'teal'*/} 
            ribbonSide {/*option for show ribbon default is left*/}
            upload {/*option for show upload button*/}
          />
        </Grid.Column>
        <Grid.Column>
          <BlobImage 
            token={token} 
            account='ACCOUNT_NAME'            
            container='CONTAINER_NAME'
            name='FILE_NAME'
            width = '100%' {/*Percent or Pixel*/} 
            height = '100%' {/*Percent or Pixel*/} 
            ribbonDescription = 'Image Description' {/*Example ''รูปที่ 2 ทดสอบ''*/} 
            ribbonColor = 'COLOR_LIBBON' {/*Example 'red'*/} 
            ribbonSide ='right' {/*option for show ribbon default is left*/}
            upload {/*option for show upload button*/}
          />
        </Grid.Column>
      </Grid>
    );     
  }
}
```

#withDocumentFetch
```jsx
import React, { Component } from 'react';
import { Form } from 'semantic-ui-react'
import validate from 'validate.js'
import { withDocumentFetch } from 'react-inforvation-utility';


validate.validators.custom = function(value, options, key, attributes) {
  console.log(value);
  console.log(options);
  console.log(key);
  console.log(attributes);
  return "is totally wrong";
};

const constraint = {
  year:{custom:{is:4}}
}

class TestDocFetch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validateError:{}
    }
  }  
  
  handleChange = (e,{name,value}) => {
    let tmp = { ...this.props.data };    
    tmp[name]=value;              
    this.props.onDocumentChange(tmp);    
  }
  
  render() {    
    const { data, validateError } = this.props;      
    console.log(data);    
    console.log(validateError);
    return (
    <Form>
      <Form.Input name="year" 
        label="year"
        error={'year' in validateError}
        value={data.year||''} onChange={this.handleChange}/> 
          {validateError['year']}
     </Form>
    );
  }
}

constraint['validator'] = validate;

export default withDocumentFetch('/obec/ject_cct',constraint)(TestDocFetch);
```

```jsx
import React, { Component,Fragment } from 'react';
import { Button } from 'semantic-ui-react'
import TestDocFetch from './TestDocFetch';


const token = "";

class Test extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }  
  
  handleValidateError = (result) => {
    console.log(result);
  }
  
  handleClick = () => {
    this.refs.form.save();
  }
  
  render() {
    return (
      <Fragment>
        <TestDocFetch docId="0011011374935" ref="form" 
          token={token} 
          onValidateError={this.handleValidateError}/>
        <Button fluid onClick={this.handleClick} content="เลือก"/>
      </Fragment>
    );
  }
}

export default Test;
```
export default withDocumentFetch('/obec/ject_cct',constraint)(TestDocFetch);
## License

NU © [theerawutt53](https://github.com/theerawutt53)
