import React, { Component,Fragment } from 'react';
import { Button } from 'semantic-ui-react'
import TestDocFetch from './TestDocFetch';


const token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiJTVTIwMTdBMDE3XzAxIiwiaWF0IjoxNTI1MzM5MzcxLCJleHAiOjE1MjUzNDY1NzF9.KYCWcQoLpC2I2TvSK2yfy90dW0GjNzBN_rWcuJcNdIi6gZyUlsGhX61oyOcUKV8J_A1W3-rglYcAejGJgJnvyhCFZTPfScVl7ZwYbMEMf693i6tgyYAnuyPjzvQxwjxnAjNJoGrrOyOObx3rWtwKPr9vgsE-PlEgIOXWzlgDd6QpsQWqW5DBkXKA_BVKTlhagM8gMzi9dl9FgmuNjLEsjS2fxhFuZTgeKOR-bFOUv0kqXokh4vNyETvXbrqud0bAKS9pnXoKFFqiCcRXW0xZZOdLRrsRDP33CWNqFs_mXhdvX1pof2Qka6YbCeCXSb8EYLNm-PoPFxmJ8a7aNPK6qg";

class Test extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }  
  
  handleValidateError = (result) => {
    console.log(result);
  }
  
  handleClick = () => {
    this.refs.form.save();
  }
  
  render() {
    return (
      <Fragment>
        <TestDocFetch docId="0011011374935" ref="form" 
          token={token} 
          onValidateError={this.handleValidateError}/>
        <Button fluid onClick={this.handleClick} content="เลือก"/>
      </Fragment>
    );
  }
}

export default Test;