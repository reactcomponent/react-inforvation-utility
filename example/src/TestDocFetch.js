import React, { Component } from 'react';
import { Form } from 'semantic-ui-react'
import validate from 'validate.js'
import { withDocumentFetch } from 'react-inforvation-utility';


validate.validators.custom = function(value, options, key, attributes) {
  console.log(value);
  console.log(options);
  console.log(key);
  console.log(attributes);
  return "is totally wrong";
};

const constraint = {
  year:{custom:{is:4}}
}

class TestDocFetch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validateError:{}
    }
  }  
  
  handleChange = (e,{name,value}) => {
    let tmp = { ...this.props.data };    
    tmp[name]=value;              
    this.props.onDocumentChange(tmp);    
  }
  
  render() {    
    const { data, validateError } = this.props;      
    console.log(data);    
    console.log(validateError);
    return (
    <Form>
      <Form.Input name="year" 
        label="year"
        error={'year' in validateError}
        value={data.year||''} onChange={this.handleChange}/> 
          {validateError['year']}
     </Form>
    );
  }
}

constraint['validator'] = validate;

export default withDocumentFetch('/obec/ject_cct',constraint)(TestDocFetch);