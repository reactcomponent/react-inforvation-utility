import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react'
import { BlobImage } from 'react-inforvation-utility'

export default class App extends Component {
  render () {    
    let token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiJTVTIwMTdBMDE3XzAxIiwiaWF0IjoxNTI0ODUzOTk2LCJleHAiOjE1MjQ4NjExOTZ9.qcIpJr9enX5i9fPxg1BsrcbAegqHNInfa5uUxkywI7QN38ZeMfVGAdtVd8dufE1nHGXEX6UxvBXz3wvAoY0q1eMtl4uAdCbGOxW28vQbvMbX4T-K4DWvVhbW0LzyD-R3HnjhOQurtfY1tVoSfVvjeOmilT1dCKxmtLgSu9RhGHhlClSgCF_dM_FXK92Amy1AwEl4_IfbGx8QooK5tA2DVHLC_DufDSLYPm5PdRvjnYVjWJqOoc0Pgh_CK19N0pTrOvju1ut8k8P9wj0CoXJkWxygevNBL1jkYQh-K5753KNffF-xbDDMx1SDjs7cvu_KXh35uOyQLOUbbRRgWWtoCg'
    return (
      <Grid columns={2}>
        <Grid.Column>
          <BlobImage 
            token={token} 
            account='obec'            
            container='cdn'
            name='bomb1'
            width = '100%' height = '100%'
            ribbonDescription = 'รูปที่ 1 ทดสอบ'
            ribbonColor = 'teal'
            ribbonSide
            upload
          />
        </Grid.Column>
        <Grid.Column>
          <BlobImage 
            token={token} 
            account='obec'            
            container='cdn'
            name='bomb2'
            width = '100%' height = '100%'
            ribbonDescription = 'รูปที่ 2 ทดสอบ'
            ribbonColor = 'red'
            ribbonSide ='right'
            upload
          />
        </Grid.Column>
      </Grid>
    );    
  }
}
