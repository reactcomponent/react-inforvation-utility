import React from 'react'
import ReactDOM from 'react-dom'

import Test from './Test'
import 'semantic-ui-css/semantic.min.css';
import './index.css'

ReactDOM.render(<Test/>, document.getElementById('root'))
