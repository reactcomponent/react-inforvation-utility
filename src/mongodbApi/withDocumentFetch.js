import React, { Component, Fragment } from 'react';
import validate from 'validate.js';

const urlServiceEndpoint = 'https://inforvation.systems/mongodb';

var savedTime;
var setAttribute = {};
    
const withDocumentFetch = (path,constraints) => WrappedComponent => {   
  
  let localValidate = validate;
  
  if(constraints.validator) {
    localValidate = constraints.validator;
    delete constraints.validator; 
  }
  
  class WithDocumentFetch extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data: {},
        validateError:{}
      }
    }

    componentDidMount() {
      const { docId,token } = this.props;
      fetch(urlServiceEndpoint + path + '/data/' + docId, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'JWT ' + token
        }
      })
      .then(response => response.json())
      .then(data => this.setState({
        data
      }))
    }

    componentWillReceiveProps(nextProps) {
      const {
        docId,
        token
      } = nextProps;
      fetch(urlServiceEndpoint + path + '/data/' + docId, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'JWT ' + token
        }
      })
      .then(response => response.json())
      .then(data => this.setState({
        data
      }))
    }
    
    

    save = (content) => {
      const { data } = this.state;
      const { token, docId } = this.props;
      this._validate(data).then(() => {         
        clearTimeout(savedTime);
        fetch(urlServiceEndpoint + path + '/data/' + docId, {
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'JWT ' + token
          },
          body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then(data => {
          if (data.ok) {
            if(this.props.onSaveSuccess) this.props.onSaveSuccess();
          } else {
            if(this.props.onSaveError) this.props.onSaveError();
          }
        })
      })
      .catch((err) => {
        this.setState({validateError:err});
      })
    }

    remove = () => {
      const {
        token,
        docId
      } = this.props;
      fetch(urlServiceEndpoint + path + '/data/' + docId, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'JWT ' + token
        }
      })
      .then(response => response.json())
      .then(data => {        
        if(this.props.onRemoved) this.props.onRemoved();
      })
    }

    update = (name, value) => {
      const { token, docId } = this.props;
      const { data } = this.state;
      data[name] = value                  
      setAttribute[name] = value
      this.setState({data}, () => {
        this._validate(setAttribute).then(() => {                       
          clearTimeout(savedTime);
          savedTime = setTimeout(() => {
            fetch(urlServiceEndpoint + path + '/data/' + docId, {
                method: 'post',
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': 'JWT ' + token
                },
                body: JSON.stringify({
                  $set: setAttribute
                })
              })
              .then(response => response.json())
              .then(data => {
                if (data.ok) {
                  setAttribute = {}
                  if(this.props.onSaveSuccess) this.props.onSaveSuccess()
                } else {
                  if(this.props.onSaveError) this.props.onSaveError()
                }
              })
          }, 5000)        
        }).catch((err) => {
          this.setState({validateError:err});
        })
      })
    }

    _validate = (data) => {            
      return new Promise((resolve, reject) => {        
        let _constraints = constraints ? constraints : {};                      
        let result = localValidate(data,_constraints);              
        if(!result) resolve();
        else reject(result);
      });      
      // return this.refs.form.validate();
    }

    onDocumentChange = (doc) => {
      const { data } = this.state;
      var merged = { ...data,...doc };
      this._validate(merged).then(() => {                       
        clearTimeout(savedTime)
        this.setState({
          'data': merged,
          'validateError':{}
        });
      }).catch((err) => {
        this.setState({'data': merged,validateError:err});
      })
    }

    render() {            
      return <WrappedComponent
        ref = "form" { ...this.props } { ...this.state }        
        onDocumentChange={ this.onDocumentChange }/>
    }
  }
  return WithDocumentFetch;
};

export default withDocumentFetch;